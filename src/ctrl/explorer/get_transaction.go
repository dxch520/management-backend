/*
Package explorer comment
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package explorer

import (
	"fmt"
	"management_backend/src/ctrl/ca"
	"management_backend/src/db"
	"management_backend/src/global"
	"strings"

	"github.com/emirpasic/gods/lists/arraylist"
	"github.com/gin-gonic/gin"

	"management_backend/src/ctrl/common"
	"management_backend/src/db/chain"
	dbcommon "management_backend/src/db/common"
	"management_backend/src/db/contract"
	contractRecord "management_backend/src/db/contract"
	"management_backend/src/entity"

	"github.com/ethereum/go-ethereum/accounts/abi"
)

// GetTxListHandler getTxListHandler
type GetTxListHandler struct {
}

// LoginVerify login verify
func (handler *GetTxListHandler) LoginVerify() bool {
	return true
}

// Handle deal
func (handler *GetTxListHandler) Handle(user *entity.User, ctx *gin.Context) {
	var (
		txList     []*dbcommon.Transaction
		totalCount int64
		offset     int
		limit      int
		addr       string
	)
	params := BindGetTxListHandler(ctx)
	if params == nil || !params.IsLegal() {
		common.ConvergeFailureResponse(ctx, common.ErrorParamWrong)
		return
	}
	offset = params.PageNum * params.PageSize
	limit = params.PageSize
	if params.ContractName != "" {
		contractInfo, err := contract.GetContractByName(params.ChainId, params.ContractName)
		if err != nil {
			common.ConvergeHandleFailureResponse(ctx, err)
			return
		}
		addr = contractInfo.EvmAddress
	}

	totalCount, txList, err := chain.GetTxList(params.ChainId, offset, limit, params.BlockHeight,
		params.ContractName, addr)
	if err != nil {
		common.ConvergeHandleFailureResponse(ctx, err)
		return
	}
	txInfos := convertToTxViews(txList)
	common.ConvergeListResponse(ctx, txInfos, totalCount, nil)
}

func convertToTxViews(txList []*dbcommon.Transaction) []interface{} {
	txViews := arraylist.New()
	for _, tx := range txList {
		contract, err := contractRecord.GetContractByNameOrEvmAddress(tx.ChainId, tx.ContractName, tx.ContractName)
		if err != nil {
			log.Errorf("GetContractByNameOrEvmAddress err: %v", err.Error())
		} else {
			if contract != nil {
				tx.ContractName = contract.Name
			}
		}
		record, err := contractRecord.GetInvokeRecordByTxId(tx.TxId)
		if err != nil {
			log.Errorf("GetInvokeRecordByTxId error: %v", err.Error())
		} else {
			if record != nil {
				tx.ContractMethod = record.Method
			}
		}
		txView := NewTransactionListView(tx)
		txViews.Add(txView)
	}
	return txViews.Values()
}

// GetTxDetailHandler getTxDetailHandler
type GetTxDetailHandler struct {
}

// LoginVerify login verify
func (handler *GetTxDetailHandler) LoginVerify() bool {
	return true
}

// Handle deal
func (handler *GetTxDetailHandler) Handle(user *entity.User, ctx *gin.Context) {
	params := BindGetTxDetailHandler(ctx)
	if params == nil || !params.IsLegal() {
		common.ConvergeFailureResponse(ctx, common.ErrorParamWrong)
		return
	}

	var (
		tx  *dbcommon.Transaction
		err error
	)

	if params.Id != 0 {
		tx, err = chain.GetTxById(params.Id)
		if err != nil {
			common.ConvergeHandleFailureResponse(ctx, err)
			return
		}
	} else if params.TxId != "" {
		tx, err = chain.GetTxByTxId(params.ChainId, params.TxId)
		if err != nil {
			common.ConvergeHandleFailureResponse(ctx, err)
			return
		}
	}

	contract, err := contractRecord.GetContractByNameOrEvmAddress(tx.ChainId, tx.ContractName, tx.ContractName)
	if err != nil {
		log.Errorf("GetContractByNameOrEvmAddress error: %v", err.Error())
	}
	// EVM
	if contract != nil && contract.RuntimeType == global.EVM {
		id, userId, hash, err := ca.ResolveUploadKey(contract.EvmAbiSaveKey)
		if err != nil {
			log.Errorf("ResolveUploadKey error: %v", err.Error())
		}
		upload, err := db.GetUploadByIdAndUserIdAndHash(id, userId, hash)
		if err != nil {
			log.Errorf("GetUploadByIdAndUserIdAndHash error: %v", err.Error())
		}
		contractAbi, err := abi.JSON(strings.NewReader(string(upload.Content)))
		if err != nil {
			log.Errorf("abi json error: %v", err.Error())
		}

		record, err := contractRecord.GetInvokeRecordByTxId(tx.TxId)
		if err != nil {
			log.Errorf("GetInvokeRecordByTxId error: %v", err.Error())
		} else {
			if tx.ContractResult != nil && len(tx.ContractResult) > 0 {
				unpackedData, err := contractAbi.Unpack(record.Method, tx.ContractResult)
				if err != nil {
					log.Errorf("abi unpack error: %v, method: %v", err.Error(), record.Method)
				} else {
					tx.ContractResult = []byte(fmt.Sprintf("%v", unpackedData))
				}
			}
		}
	}
	txView := NewTransactionView(tx)
	common.ConvergeDataResponse(ctx, txView, nil)
}
